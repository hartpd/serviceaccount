﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Security.Principal;


namespace ServiceAccounts
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private List<Tuple<string, string>> _serviceAccounts = new List<Tuple<string, string>>();

        public MainWindow()
        {
            InitializeComponent();

            SetGrid();
        }

        private void SetGrid()
        {
            var bindingAccounts = new List<ServiceAccount>();

            foreach (var serviceAccount in _serviceAccounts)
            {
                bindingAccounts.Add(new ServiceAccount(serviceAccount.Item1, serviceAccount.Item2));
            }

            UserDataGrid.ItemsSource = bindingAccounts;
        }

        class ServiceAccount
        {
            public ServiceAccount(string environmentName, string accountName)
            {
                Environment = environmentName;
                AccountName = accountName;
            }

            public string Environment { get; }
            public string AccountName { get; }

            public bool AccountExists
            {
                get
                {
                    var toReturn = false;

                    try
                    {
                        NTAccount acct = new NTAccount(AccountName);
                        SecurityIdentifier id = (SecurityIdentifier)acct.Translate(typeof(SecurityIdentifier));

                        toReturn = id.IsAccountSid();
                    }
                    catch (IdentityNotMappedException)
                    {
                        /* Invalid user account */
                    }

                    return toReturn;
                }
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            _serviceAccounts.Add(new Tuple<string, string>(EnvironmentBox.Text, UserBox.Text));

            EnvironmentBox.Text = string.Empty;
            UserBox.Text = string.Empty;

            SetGrid();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            foreach (var selectedItem in UserDataGrid.SelectedItems.Cast<ServiceAccount>())
            {
                var itemToRemove =
                    _serviceAccounts.Where(
                        i => i.Item1 == selectedItem.Environment && i.Item2 == selectedItem.AccountName)
                        .ToList();

                for (int i = 0; i < itemToRemove.Count(); i++)
                {
                    var item = itemToRemove[i];

                    _serviceAccounts.Remove(item);
                }
            }

            SetGrid();
        }
    }
}
